import os
import sys
import json
import subprocess

user_path = ''
paths = {} 
paths['versions'] = []

def DisplaySavedExecPaths():
    ctr = 0
    with open('exec_paths.json') as json_file:  
        data = json.load(json_file)
        for v in data['versions']:
            print('[{}]. '.format(ctr+1) + v['path'])

def DisplayActions():
    print('[A]. Add')
    DisplaySavedExecPaths()
    print('[X]. Exit')
    print('\n\n')

def SelectAction():
    DisplayActions()
    action = str(input('Select action : '))
    if action == 'a' or action == 'A':
        os.system('cls')
        AddExecPath()
    elif action == 'x' or action == 'X':
        quit()
    else:
        os.system('cls')
        GetExecPath(int(action)- 1)

def GetExecPath(version):
    with open('exec_paths.json') as json_file:  
        data = json.load(json_file)
        print(data['versions'][version]['path'])
        exec_path = data['versions'][version]['path']
        
        RunExecPath(exec_path)

def RunExecPath(exec_path):
    subprocess.call(str(exec_path))

    main()

def AddExecPath():
    user_path = str(input('Enter executable path ( use forward slashes (/): '))
    paths['versions'].append({ 'path' : user_path })

    with open('exec_paths.json', 'w') as outfile:
        json.dump(paths, outfile)
    
    print('Path added!')
    main()

def main():
    SelectAction()

main()